/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bomba;

/**
 *
 * @author frida victoria
 */

//atributos

public class Gasolina {
    private int idGasolina;
    private String tipo;
    private String marca;
    private float precio;


//constructores

    
    public Gasolina(int idGasolina, String tipo, String marca, float precio) {
        this.idGasolina = idGasolina;
        this.tipo = tipo;
        this.marca = marca;
        this.precio = precio;
    }
    
    public Gasolina() {
        this.idGasolina = 0;
        this.tipo = "";
        this.marca = "";
        this.precio = 0.0f;
    }

   //encapsulamiento

    public int getIdGasolina() {
        return idGasolina;
    }

    public void setIdGasolina(int idGasolina) {
        this.idGasolina = idGasolina;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
   
    public String obtenerInformacion(){

        return "Id: " + this.idGasolina + "Marca: " + this.marca + "Tipo: " + this.tipo + "Precio: " + this.precio;

    }
    
}

    