/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bomba;

/**
 *
 * @author frida victoria
 */
public class Bomba {
    private int idBomba;
    private int contador;
    private int capacidad;
    private Gasolina gasolina;
    
    //constructores

     public Bomba() {
        this.idBomba = 0;
        this.contador = 0;
        this.capacidad = 0;
        this.gasolina = new Gasolina();
    }
     
    public Bomba(int idBomba, int contador, int capacidad, Gasolina gasolina) {
        this.idBomba = idBomba;
        this.contador = contador;
        this.capacidad = capacidad;
        this.gasolina = gasolina;
    }

    //encapsulamiento

    public int getIdBomba() {
        return idBomba;
    }

    public void setIdBomba(int idBomba) {
        this.idBomba = idBomba;
    }

    public int getContador() {
        return contador;
    }

    public void setContador(int contador) {
        this.contador = contador;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public Gasolina getGasolina() {
        return gasolina;
    }

    public void setGasolina(Gasolina gasolina) {
        this.gasolina = gasolina;
    }
    
    //comportamiento
    
    public float obtenerInventario(){
        return this.capacidad - this.contador;
    }
    
    public float venderGasolina (int cantidad ){
        float totalVenta = 0.0f;
        
        if(cantidad <= this.obtenerInventario()){
            totalVenta = cantidad*this.gasolina.getPrecio();
            this.contador = this.contador + cantidad;
        }
        return totalVenta;
        
    }
    
    public float totalVenta(){
        return this.contador * this.gasolina.getPrecio();
    }
    
   
}
