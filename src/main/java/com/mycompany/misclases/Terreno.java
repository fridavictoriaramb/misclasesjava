package com.mycompany.misclases;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author frida victoria
 */
public class Terreno {
    private int numLote;
    private float metrosAncho;
    private float metrosLargo;
    
    //constructores

    public Terreno() {
        this.numLote = 0;
        this.metrosAncho = 0.0f;
        this.metrosAncho = 0.0f;
    }

    public Terreno(int numLote, float metrosAncho, float metrosLargo) {
        this.numLote = numLote;
        this.metrosAncho = metrosAncho;
        this.metrosLargo = metrosLargo;
    }
    
    public Terreno(Terreno t) {
        this.numLote = t.numLote;
        this.metrosAncho = t.metrosAncho;
        this.metrosLargo = t.metrosLargo;
    }
    
    //encapsulamiento

    public int getNumLote() {
        return numLote;
    }

    public void setNumLote(int numLote) {
        this.numLote = numLote;
    }

    public float getMetrosAncho() {
        return metrosAncho;
    }

    public void setMetrosAncho(float metrosAncho) {
        this.metrosAncho = metrosAncho;
    }

    public float getMetrosLargo() {
        return metrosLargo;
    }

    public void setMetrosLargo(float metrosLargo) {
        this.metrosLargo = metrosLargo;
    }
    
    
    //metodos de comportamienti
    
    public float calcularPerimetro () {
        return this.metrosAncho * 2 + this.metrosLargo;
    }
    
    public float calcularArea () {
        return this.metrosAncho * this.metrosLargo;
        
    }
}
