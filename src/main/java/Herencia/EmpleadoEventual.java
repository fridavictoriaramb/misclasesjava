/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Herencia;

/**
 *
 * @author frida victoria
 */
public class EmpleadoEventual extends Empleado{
    
    private float pagoHoras;
    private float horasTrabajadas;

    //constructores
    
    public EmpleadoEventual(float pagoHoras, float horasTrabajadas, int numEmpleado, String nombre, String puesto, String depto) {
        super(numEmpleado, nombre, puesto, depto);
        this.pagoHoras = pagoHoras;
        this.horasTrabajadas = horasTrabajadas;
    }

    public EmpleadoEventual() {
        super();
        this.horasTrabajadas = 0.0f;
        this.pagoHoras = 0.0f;
    }

    // get and set
    
    public float getPagoHoras() {
        return pagoHoras;
    }

    public void setPagoHoras(float pagoHoras) {
        this.pagoHoras = pagoHoras;
    }

    public float getHorasTrabajadas() {
        return horasTrabajadas;
    }

    public void setHorasTrabajadas(float horasTrabajadas) {
        this.horasTrabajadas = horasTrabajadas;
    }
    
    
    @Override
    public float calcularPago() {
    float pago = 0.0f;
    pago = this.horasTrabajadas * this.pagoHoras;
    return pago;
    
    }
}
