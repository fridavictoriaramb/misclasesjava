/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Herencia;

/**
 *
 * @author frida victoria
 */
public class EmpleadoBase extends Empleado implements Impuestos{
    private float pagoDiario;
    private float diasTrabajados;
    
    //constructores
//por argumentos   

    public EmpleadoBase(float pagoDiario, float diasTrabajados, int numEmpleado, String nombre, String puesto, String depto) {
        super(numEmpleado, nombre, puesto, depto);
        this.pagoDiario = pagoDiario;
        this.diasTrabajados = diasTrabajados;
    }
    
//por omision 
    public EmpleadoBase(){
        super();
        this.diasTrabajados = 0.0f;
        this.pagoDiario = 0.0f;
    }

    // get and set
    
    public float getPagoDiario() {
        return pagoDiario;
    }

    public void setPagoDiario(float pagoDiario) {
        this.pagoDiario = pagoDiario;
    }

    public float getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(float diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }


    @Override
    public float calcularPago() {
        return this.diasTrabajados * this.pagoDiario;
    }

    @Override
    public float calcularImpuestos() {
        float pago = 0.0f;
        if (this.calcularPago()>=15000) pago = this.calcularPago()*.15f;
        return pago;
    }
    
}
