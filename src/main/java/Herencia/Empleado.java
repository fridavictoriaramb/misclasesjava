/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Herencia;

/**
 *
 * @author frida victoria
 */

public abstract class Empleado {
    protected int numEmpleado;
    protected String nombre;
    protected String puesto;
    protected String depto;
    
    //constructores
    
// por argumentos
    public Empleado(int numEmpleado, String nombre, String puesto, String depto) {
        this.numEmpleado = numEmpleado;
        this.nombre = nombre;
        this.puesto = puesto;
        this.depto = depto;
    }
//por omision
    public Empleado() {
        this.numEmpleado = 0;
        this.nombre = "";
        this.puesto = "";
        this.depto = "";
    }
    
    
    //set and get
    
    public int getNumEmpleado() {
        return numEmpleado;
    }

    public void setNumEmpleado(int numEmpleado) {
        this.numEmpleado = numEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getDepto() {
        return depto;
    }

    public void setDepto(String depto) {
        this.depto = depto;
    }
    
    public abstract float calcularPago();
    
    
}


